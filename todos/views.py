from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.

def show_todo_list(request):
    todo_lists = TodoList.objects.all()
    context = {
        'todo_lists': todo_lists,
    }
    return render(request, 'todos/list.html', context)


def show_todo_detail(request,id):
   todo_list = get_object_or_404(TodoList, id=id)
   return render(request, 'todos/detail.html', {'todo_list': todo_list})

def create_todo_list(request):
  if request.method == "POST":
    form = TodoListForm(request.POST)
    if form.is_valid():

      todo_list = form.save()
      return redirect('todo_list_detail', id=todo_list.id)

  else:
    form = TodoListForm()

  context = {
    "form": form
  }

  return render(request, "todos/create.html", context)

def update_todo_list_name(request, id):
  todo_list = TodoList.objects.get(id=id)
  if request.method == "POST":
    form = TodoListForm(request.POST, instance=todo_list)
    if form.is_valid():
      todo_list = form.save()
      return redirect("todo_list_detail", id=todo_list.id)

      # To add something to the model, like setting a user,
      # use something like this:
      #
      # model_instance = form.save(commit=False)
      # model_instance.user = request.user
      # model_instance.save()
      # return redirect("detail_url", id=model_instance.id)
  else:
    form = TodoListForm(instance=todo_list)

  context = {
    "form": form
  }

  return render(request, "todos/edit.html", context)

def delete_todo_list(request, id):
  todo_list = TodoList.objects.get(id=id)
  if request.method == "POST":
    todo_list.delete()
    return redirect("todo_list_list")

  return render(request, "todos/delete.html")


def create_todo_item(request):
  if request.method == "POST":
    form = TodoItemForm(request.POST)
    if form.is_valid():

      todo_item = form.save()
      return redirect('todo_list_detail', id=todo_item.list.id)

  else:
    form = TodoItemForm()

  context = {
    "form": form
  }

  return render(request, "todos/createitem.html", context)

def edit_todo_item(request, id):
  todo_item = TodoList.objects.get(id=id)
  if request.method == "POST":
    form = TodoItemForm(request.POST, instance=todo_item)
    if form.is_valid():
      todo_item = form.save()
      return redirect("todo_list_detail", id=todo_item.list.id)

      # To add something to the model, like setting a user,
      # use something like this:
      #
      # model_instance = form.save(commit=False)
      # model_instance.user = request.user
      # model_instance.save()
      # return redirect("detail_url", id=model_instance.id)
  else:
    form = TodoItemForm(instance=todo_item)

  context = {
    "form": form
  }

  return render(request, "todos/edititem.html", context)
